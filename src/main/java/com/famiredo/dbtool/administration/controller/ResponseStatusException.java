package com.famiredo.dbtool.administration.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class ResponseStatusException extends RuntimeException {

    public ResponseStatusException(HttpStatus status, String message) {
        super(message);
    }
}

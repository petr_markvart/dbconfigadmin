package com.famiredo.dbtool.administration.controller;

import com.famiredo.dbtool.administration.persistence.DbConfigRepository;
import com.famiredo.dbtool.administration.persistence.model.DbColumn;
import com.famiredo.dbtool.administration.persistence.model.DbConfig;
import com.famiredo.dbtool.administration.persistence.model.DbSchema;
import com.famiredo.dbtool.administration.persistence.model.DbTable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.jdbc.CannotGetJdbcConnectionException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Ataccama - Task 2
 *
 * TODO: This version is tested for MySQL server DB connections ONLY.
 * TODO: Use strategy pattern to use correct SQL version - Queries are different for each DB type (MSSQL, ORACLE...)
 *
 */
@Controller
@RequestMapping(path="/task2") // URL's start with /administration (after Application path)
public class DbAnalyticsController {

  private static final Logger LOGGER = LoggerFactory.getLogger(DbAnalyticsController.class);

  @Autowired
  JdbcTemplate jdbcTemplate;

  @Autowired
  private DbConfigRepository dbConfigRepository;

  /**
   * Get list of schema from database
   *
   * @param id id from configuration table (db_config) where is info about DB connection
   * @return
   */
  @GetMapping(path="/dbschema")
  public @ResponseBody Iterable<DbSchema> getDbInfoSchema(@RequestParam int id) {
    LOGGER.info("call /dbschema with params id={}", id);
    setupDataSource(id);
    try {
      return jdbcTemplate.query(
              "select schema_name, catalog_name, default_character_set_name as charset " +
                      "from information_schema.schemata " +
                      "order by schema_name",
              (rs, rowNum) ->
                      new DbSchema(rs.getString("schema_name"), rs.getString("catalog_name"), rs.getString("charset"))
      );
    } catch (CannotGetJdbcConnectionException ex) {
      throw new ResponseStatusException(HttpStatus.NOT_FOUND,"Connection to DB failed. See configuration for ID=" + id);
    }
  }

  /**
   * Get list of tables for concrete schema
   *
   * @param id id from configuration table (db_config) where is info about DB connection
   * @param schema name of schema from where will be listed tables
   * @return
   */
  @GetMapping(path="/dbtable")
  public @ResponseBody Iterable<DbTable> getDbInfoTable(@RequestParam int id, @RequestParam String schema) {
    LOGGER.info("call /dbtable with params id={}, schema={}", id, schema);
    setupDataSource(id);
    return jdbcTemplate.query(
            "show tables from " + schema,
            (rs, rowNum) ->
                    new DbTable(rs.getString(1))
    );
  }

  /**
   * Show all columns for selected table (from selected schema)
   *
   * @param id id from configuration table (db_config) where is info about DB connection
   * @param schema name of schema from where will be listed tables
   * @param table table for where is listed column info
   * @return
   */
  @GetMapping(path="/dbcolumn")
  public @ResponseBody Iterable<DbColumn> getDbInfoColumn(@RequestParam int id,
                                                        @RequestParam String schema,
                                                        @RequestParam String table) {
    LOGGER.info("call /dbcolumn with params id={}, schema={}, table={}", id, schema, table);
    setupDataSource(id);
    return jdbcTemplate.query(
            "show columns from " + schema + "." + table,
            (rs, rowNum) ->
                    new DbColumn(rs.getString("Field"), rs.getString("Type"), rs.getString("Null"),
                            rs.getString("Key"), rs.getString("Default"), rs.getString("Extra"))
    );
  }

  /**
   * Get firs 5 rows from table
   *
   * @param id id from configuration table (db_config) where is info about DB connection
   * @param schema name of schema from where will be listed table
   * @param table table for where is listed content
   * @return
   */
  @GetMapping(path="/dbtablecontent")
  public @ResponseBody Iterable<List<String>> getDbInfoTableContent(@RequestParam int id,
                                                                    @RequestParam String schema,
                                                                    @RequestParam String table) {
    LOGGER.info("call /dbtablecontent with params id={}, schema={}, table={}", id, schema, table);
    setupDataSource(id);
    return jdbcTemplate.query(
            "select * from " + schema + "." + table + " limit 5",
            (rs, rowNum) ->
            {
              List<String> rowAsList = new ArrayList<>();
              for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                rowAsList.add(rs.getString(i));
              }
              return rowAsList;
            }
    );
  }


  private void setupDataSource(@RequestParam int id) {
    Optional<DbConfig> dbConfig = dbConfigRepository.findById(id);
    if (dbConfig.isPresent()) {
      String url = "jdbc:mysql://" +
              dbConfig.get().getHostname() + ":" +
              dbConfig.get().getPort() + "/" +
              dbConfig.get().getDatabaseName() +
              "?useUnicode=true&serverTimezone=UTC";
      DataSource ds = DataSourceBuilder
              .create()
              .url(url)
              .username(dbConfig.get().getUsername())
              .password(dbConfig.get().getPassword())
              .build();
      jdbcTemplate.setDataSource(ds);
    } else {
      throw new ResponseStatusException(HttpStatus.NO_CONTENT, "DB configuration defined by ID is not found. ID=" + id);
    }
  }

  /**
   * Get list of all DB configurations
   *
   * @return json implementation of db_config
   */
  @GetMapping(path="/all")
  public @ResponseBody Iterable<DbConfig> getAllUsers() {
    LOGGER.info("Get list of all db connections.");
    return dbConfigRepository.findAll();
  }


}
package com.famiredo.dbtool.administration.controller;

import com.famiredo.dbtool.administration.persistence.DbConfigRepository;
import com.famiredo.dbtool.administration.persistence.model.DbConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@Controller
public class DbConfigController {

    @Autowired
    private DbConfigRepository dbConfigRepository;

    @RequestMapping(value="/list-db-config")
    public String getAllDbConfig(ModelMap model) {
        model.put("dbconfigs", dbConfigRepository.findAll());
        return "list-db-config";
    }

    @GetMapping("/list-db-delete")
    public String deleteItem(@RequestParam(name="id", required=true) int id) {
        dbConfigRepository.deleteById(id);
        return "redirect:/list-db-config";
    }

    @GetMapping("/list-db-update")
    public String updateItemForm(@RequestParam(name="id", required=true) int id, Model model) {
        Optional<DbConfig> dbConfig = dbConfigRepository.findById(id);
        if (dbConfig.isPresent()) {
            model.addAttribute("dbconfig", dbConfig.get());
        }
        return "list-db-add";
    }

    @PostMapping("/list-db-update")
    public String updateItemSave(@Valid @ModelAttribute("dbconfig") DbConfig dbConfig, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "list-db-add";
        }
        dbConfigRepository.save(dbConfig);
        return "redirect:/list-db-config";
    }

    @GetMapping("/list-db-add")
    public String addItemForm(Model model) {
        model.addAttribute("dbconfig", new DbConfig());
        return "list-db-add";
    }

}

package com.famiredo.dbtool.administration.persistence.model;

public class DbColumn {
    private String colField;
    private String colType;
    private String colNull;
    private String colKey;
    private String colDefault;
    private String colExtra;

    public DbColumn(String colField, String colType, String colNull, String colKey, String colDefault, String colExtra) {
        this.colField = colField;
        this.colType = colType;
        this.colNull = colNull;
        this.colKey = colKey;
        this.colDefault = colDefault;
        this.colExtra = colExtra;
    }

    public String getColField() {
        return colField;
    }

    public void setColField(String colField) {
        this.colField = colField;
    }

    public String getColType() {
        return colType;
    }

    public void setColType(String colType) {
        this.colType = colType;
    }

    public String getColNull() {
        return colNull;
    }

    public void setColNull(String colNull) {
        this.colNull = colNull;
    }

    public String getColKey() {
        return colKey;
    }

    public void setColKey(String colKey) {
        this.colKey = colKey;
    }

    public String getColDefault() {
        return colDefault;
    }

    public void setColDefault(String colDefault) {
        this.colDefault = colDefault;
    }

    public String getColExtra() {
        return colExtra;
    }

    public void setColExtra(String colExtra) {
        this.colExtra = colExtra;
    }
}

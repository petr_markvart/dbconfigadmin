package com.famiredo.dbtool.administration.persistence.model;

public class DbSchema {

    private String schemaName;
    private String catalogName;
    private String charSet;

    public DbSchema(String schemaName, String catalogName, String charSet) {
        this.schemaName = schemaName;
        this.catalogName = catalogName;
        this.charSet = charSet;
    }

    public String getSchemaName() {
        return schemaName;
    }

    public void setSchemaName(String schemaName) {
        this.schemaName = schemaName;
    }

    public String getCatalogName() {
        return catalogName;
    }

    public void setCatalogName(String catalogName) {
        this.catalogName = catalogName;
    }

    public String getCharSet() {
        return charSet;
    }

    public void setCharSet(String charSet) {
        this.charSet = charSet;
    }

    @Override
    public String toString() {
        return "DbSchema{" +
                "schemaName='" + schemaName + '\'' +
                ", catalogName='" + catalogName + '\'' +
                ", charSet='" + charSet + '\'' +
                '}';
    }
}

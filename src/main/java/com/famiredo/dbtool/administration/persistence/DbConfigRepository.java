package com.famiredo.dbtool.administration.persistence;

import com.famiredo.dbtool.administration.persistence.model.DbConfig;
import org.springframework.data.repository.CrudRepository;

public interface DbConfigRepository extends CrudRepository<DbConfig, Integer> {
    
}

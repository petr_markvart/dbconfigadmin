package com.famiredo.dbtool.administration.persistence.model;

import org.springframework.format.annotation.NumberFormat;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * Entity will keep information about DB connection
 *
 *
 * TODO: add more detailed validation for data insertion by FORM
 *
 * TODO: add more info about DB connection. DB type (like MySQL, Oracle, MSSQL)
 * TODO: - it is needed because of later use in DbAnalyticsController
 */
@Entity
public class DbConfig {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Size(min=1, max=255)
    private String name; // ​ - custom name of the database instance

    @Pattern(regexp = "^[\\.a-z0-9]+$", message = "Define hostname (like localhost, db1.oracle.com etc.)")
    private String hostname; // ​ - hostname of the database

    @NumberFormat
    @Size(min=1, max=5)
    private String  port; // ​ - port where the database runs

    @Size(min=1, max=255)
    private String  databaseName; // ​ - name of the database

    @Size(min=1, max=255)
    private String  username; // ​ - username for connecting to the database

    @Size(max=255)
    private String  password; // ​ - password for connecting to the database

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getDatabaseName() {
        return databaseName;
    }

    public void setDatabaseName(String databaseName) {
        this.databaseName = databaseName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "DbConnectionConfig{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", hostname='" + hostname + '\'' +
                ", port='" + port + '\'' +
                ", databaseName='" + databaseName + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}

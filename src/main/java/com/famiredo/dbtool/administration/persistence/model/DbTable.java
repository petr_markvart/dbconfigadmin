package com.famiredo.dbtool.administration.persistence.model;

public class DbTable {

    private String tableName;

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public DbTable(String tableName) {
        this.tableName = tableName;
    }
}

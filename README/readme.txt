Project description is in PDF file:
src/main/resources/static/task-info.pdf

You need MySQL version 8.0.19

To define DB connection string update following file:
src/main/resources/application.properties

When you compile and run application (main class AdministrationApplication), you can try:

http://localhost:8080/list-db-config
- create new records for DB configuration, see list of current

http://localhost:8080/task2/all
- see all current configurations (use id in other requests)

http://localhost:8080/task2/dbschema?id=22
- use ID of DB configuration to read all its schema

http://localhost:8080/task2/dbtable?id=22&schema=sys
- see all tables from concrete schema

http://localhost:8080/task2/dbcolumn?id=22&schema=sys&table=user_summary
- see all column info for concrete schema and table

http://localhost:8080/task2/dbtablecontent?id=22&schema=sys&table=user_summary
- see top 5 rows from concrete table